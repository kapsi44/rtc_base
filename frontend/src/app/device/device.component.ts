import {Component, OnInit, ViewChild, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Column, CdtSettings, DataTable, DataSource, DataManager} from '../../ng-crud-table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {DeviceService} from '../_services/device/device.service';
import {getDeviceDetails, getTestDetails} from './columns';
import {environment, deviceData} from '../../environments/environment';
import { CustomFilterComponent } from './custom-filter/customFilter.component';
import {FilterDialogBoxComponent, DeleteDialogBoxComponent} from './dialog-box/dialogBox.component';
import {FILTERS, Filter} from './filters';
import {ViewEncapsulation} from '@angular/core';
import { Globals } from '../globals';



@Component({
  selector: 'app-virtual-scroll-demo',
  templateUrl: './device.component.html',
  styleUrls: ['device.css'],
  encapsulation: ViewEncapsulation.None
})

export class DeviceComponent implements OnInit {

  @ViewChild(CustomFilterComponent)
  customFilter: CustomFilterComponent;

  @ViewChild(FilterDialogBoxComponent)
  saveFilterComp: FilterDialogBoxComponent;

  @ViewChild(DeleteDialogBoxComponent)
  deleteFilterComp: DeleteDialogBoxComponent;


  panelOpenState = false;
  table: DataTable;
  columns: Column[];
  service: DataSource;
  dataManager: DataManager;
  filter: Filter;
  query: any;
  getFilters: any;
  selectedFilter: any;

  settings: CdtSettings = <CdtSettings>{
    virtualScroll: true,
    paginator: false,
  };

  serverSideSettings: CdtSettings = <CdtSettings>{
    virtualScroll: true,
    paginator: false,
    bodyHeight: 400,
    globalFilter: true
  };

  constructor(
      private http: HttpClient,
      public dialog: MatDialog,
      private globals: Globals
    ) {
    this.columns = getTestDetails();
    this.getSavedFilters();
    this.service = new DeviceService(this.http);
    this.service.url = deviceData.apiUrl;
    this.dataManager = new DataManager(this.columns, this.serverSideSettings, this.service);
  }


  ngOnInit() {
    this.getTableData();
  }

  /*
  * To request for the related data for the table
  */
  getTableData(query?: string) {
    this.dataManager.events.onLoading(true);
    this.http.get<any[]>(this.filterApi(query)).subscribe(data => {
      for (const row of data) {
        row.$$height = (row.exp > 1000000) ? 40 : 25;
      }
      this.dataManager.rows = data;
      this.dataManager.events.onLoading(false);
    });
  }

  /*
  * Returns the api as per the condition
  */
  filterApi(query?: string): any {
      return (this.customFilter.filterArray === undefined) ? deviceData.apiUrl :
             ((query) ? deviceData.apiUrl + '?data=' + query :
             deviceData.apiUrl + '?data=' + this.modifyApi());
  }

  /*
  * Builds the api as per the filters selected
  */
  modifyApi(): any {
    const data = [{}];
    const filters = FILTERS['All Devices'];

    // assign filterArray value from selected options in custom filter component
    const selectedArray = this.customFilter.filterArray;

    let filteredArray;

    if (filters.Platform.some(r => selectedArray.indexOf(r) > -1)) {
      const platform = data[0]['platform'] = [];
      filteredArray = this.customFilter.addFilteredItems(filters.Platform, selectedArray);
      platform.push.apply(platform, filteredArray);
    }
    if (filters.Status.some(r => selectedArray.indexOf(r) > -1)) {
      const status = data[0]['status'] = [];
      filteredArray = this.customFilter.addFilteredItems(filters.Status, selectedArray);
      status.push.apply(status, filteredArray);
    }
    if (filters.Country.some(r => selectedArray.indexOf(r) > -1)) {
      const country = data[0]['country'] = [];
      filteredArray = this.customFilter.addFilteredItems(filters.Country, selectedArray);
      country.push.apply(country, filteredArray);
    }

    return JSON.stringify(data);
  }

 /*
 * Returns the data as per the saved filters
 */
  filterSavedQuery(query?: string) {
    this.getTableData(query);
  }

  /*
  * Opens a dialog to save the requested filter
  */
  saveFilterDialog(): void {
    this.query = this.modifyApi();
    const dialogRef = this.dialog.open(FilterDialogBoxComponent, {
      width: '500px',
      data: {name: this.query},
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) { this.saveFilters(result); }
    });
  }

  /*
  * Opens a dialog to delete the requested filter
  */
  deleteFilterDialog(id?: number): void {
    this.query = this.modifyApi();
    const dialogRef = this.dialog.open(DeleteDialogBoxComponent, {
      width: '500px',
      data: {filterId: id},
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) { this.deleteFilters(result); }
    });
  }

  /*
  * Returns the saved filters details
  */
  getSavedFilters(): any {
    this.http.get(environment.apiUrl + 'api/filters/' + this.globals.user['id']).subscribe(data => {
      this.getFilters = data['data'];
    });
  }

  /*
  * Saves the requested filters
  */
  saveFilters(filterName: string): any {
    this.http.post(environment.apiUrl + 'api/filters',
    {'name': filterName, 'saved_query': this.query, 'user_id': this.globals.user['id']
    }).subscribe(data => {
      this.getSavedFilters();
    });
  }

  /*
  * Deletes the requested saved filters query
  */
  deleteFilters(id?: number) {
    this.http.delete(environment.apiUrl + 'api/filters/' + id).subscribe(data => {
      this.getSavedFilters();
      this.getTableData();
    });
  }

  clearSortingFilters() {
    this.selectedFilter = null;
    this.getTableData();
  }
}

