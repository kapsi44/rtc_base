import {Column} from '../../ng-crud-table';
import {Validators} from '../../lib/validation/validators';

export function getDeviceDetails(): Column[] {
  const columnsDevices: Column[] = [
    {
      title: 'Device ID',
      name: 'attributes.devicename',
      sortable: true,
      filter: true,
      width: 200,
      formHidden: true,
      isPrimaryKey: true,
    },
    {
      title: 'Model',
      name: 'attributes.modelname',
      sortable: true,
      filter: true,
      width: 150,
      formHidden: true,
    },
    {
      title: 'Software Pkg.',
      name: 'attributes.softwarepackageversion',
      sortable: true,
      width: 150,
      formHidden: true,
    },
    {
      title: 'Category',
      name: 'device.platform.name',
      sortable: true,
      filter: true,
      width: 100,
      formHidden: true,
    },
    {
      title: 'Store',
      name: 'device.make',
      sortable: true,
      filter: true,
      width: 100,
      formHidden: true,
    },
    {
      title: 'Reg. Date',
      name: 'attributes.datetime',
      sortable: true,
      filter: true,
      width: 150,
      formHidden: true,
      type: 'datetime-local',
    }
  ];
  return columnsDevices;
}

export function getTestDetails(): Column[] {
  const columnsTest: Column[] = [
    {
      title: 'Device ID',
      name: 'id',
      sortable: true,
      width: 200,
      resizeable: false,
      isPrimaryKey: true,
    },
    {
      title: 'Model',
      name: 'device.model',
      sortable: true,
      resizeable: false,
      width: 150,
    },
    {
      title: 'Software Pkg.',
      name: 'application.sdk.name',
      sortable: true,
      resizeable: false,
      width: 150,
    },
    {
      title: 'Country',
      name: 'device.locale.country',
      sortable: true,
      resizeable: false,
      width: 100,
    },
    {
      title: 'Platform',
      name: 'device.platform.name',
      sortable: true,
      resizeable: false,
      width: 100,
    },
    {
      title: 'Status',
      name: 'status',
      sortable: true,
      width: 80,
      resizeable: false,
      tableHidden: false
    },
    {title: 'Event Type', name: 'event_type', tableHidden: true},
    {title: 'Package Name', name: 'application.package_name', tableHidden: true},
    {title: 'Title', name: 'application.title', tableHidden: true},
    {title: 'Version_code', name: 'application.version_code', tableHidden: true},
    {title: 'Locale Code', name: 'device.locale.code', tableHidden: true},
    {title: 'Device Make', name: 'device.make', tableHidden: true},
    {title: 'Platform Version', name: 'device.platform.version', tableHidden: true},
    {title: 'Start Time', name: 'attributes.start_timestamp', tableHidden: true}
  ];
  return columnsTest;
}

