import {Component, OnInit, ViewChild, Input, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';


export interface DialogData {
    savedFilter: string;
    name: string;
    filterName: any;
    filterId: any;
  }

@Component({
    selector: 'app-save-filter-dialog',
    templateUrl: './saveFilter.component.html',
    styleUrls: ['../device.css'],
  })
  export class FilterDialogBoxComponent {

    form: FormGroup;
    saveFilter: any;
    filterName: any;

    constructor(
      private fb: FormBuilder,
      public dialogRef: MatDialogRef<FilterDialogBoxComponent>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) {
        this.filterName = data.filterName;
        this.form = this.fb.group({
            filterName: [this.filterName, []],
        });
      }

    onNoClick(): void {
      this.dialogRef.close();
    }

    onClickMe() {
        this.dialogRef.close(this.form.get('filterName').value);
    }
}

@Component({
  selector: 'app-delete-filter-dialog',
  templateUrl: './deleteFilter.component.html',
  styleUrls: ['../device.css'],
})
export class DeleteDialogBoxComponent {

  form: FormGroup;
  filterId: any;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DeleteDialogBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.filterId = data.filterId;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onClickMe() {
      this.dialogRef.close(this.filterId);
  }
}
