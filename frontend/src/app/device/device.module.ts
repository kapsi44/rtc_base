import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { DataTableModule } from '../../ng-data-table';
import { CrudTableModule } from '../../ng-crud-table';
import {RowViewModule} from '../../lib/row-view';
import { DeviceComponent } from '../device/device.component';
import { FilterDialogBoxComponent, DeleteDialogBoxComponent } from './dialog-box/dialogBox.component';
import { CustomFilterComponent } from './custom-filter/customFilter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CdkTreeModule} from '@angular/cdk/tree';

import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatListModule,
  MatPaginatorModule,
  MatSortModule,
  MatTreeModule,
  MatDialogModule,
  MatInputModule,
  MatRadioModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CdkTreeModule,
    DataTableModule,
    CrudTableModule,
    RowViewModule,
    ReactiveFormsModule,
    // material
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatListModule,
    MatPaginatorModule,
    MatSortModule,
    MatTreeModule,
    MatDialogModule,
    FormsModule,
    MatInputModule,
    MatRadioModule
  ],
  declarations: [
      DeviceComponent,
      CustomFilterComponent,
      FilterDialogBoxComponent,
      DeleteDialogBoxComponent
    ],
  exports: [
      DeviceComponent,
      CustomFilterComponent,
      FilterDialogBoxComponent,
      DeleteDialogBoxComponent
    ],
  providers: [],
  entryComponents: [FilterDialogBoxComponent, DeleteDialogBoxComponent]
})
export class DeviceModule {}
