import {Component, OnInit, ViewChild, Input} from '@angular/core';
import {deviceData} from '../../environments/environment';


export const FILTERS =  {
  'All Devices': {
    'Platform': [
      'ANDROID',
      'IOS',
      'GOOGLE'
    ],
    'Country': [
      'US',
      'UK'
    ],
    'Status': [
      'on',
      'off'
    ]
  }
};

export class Filter {

  filters = FILTERS['All Devices'];

  isFilterable(option: any) {

  }
}
