import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EnvironmentService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  setApiService(serviceStr) {
    return this.apiUrl + 'api/' + serviceStr;
  }
  setApiServiceWithPage(serviceStr, pageNo) {
    return this.apiUrl + 'api/' + serviceStr + '?page=' + pageNo;
  }
  setApiServiceById(serviceStr, id) {
    return this.apiUrl + 'api/' + serviceStr + '/' + id;
  }
  setAuthService(serviceStr) {
    return this.apiUrl + serviceStr;
  }
  setAuthServiceById(serviceStr, id) {
    return this.apiUrl + serviceStr + '/' + id;
  }
  setLoginJson(loginData) {
    let formObject = loginData;
    formObject.client_secret = environment.clientSecret;
    formObject.grant_type = environment.grantType;
    formObject.client_id = environment.clientId;
    return formObject;
  }

  getDevices(serviceStr) {
    if (serviceStr === 'db') {
      return this.apiUrl + 'api/device';
    } else if (serviceStr === 'json') {
      return 'assets/rtc_sample.json';
    }
  }
}
