import { Component, OnInit } from '@angular/core';
import {
  SnotifyService,
  SnotifyPosition,
  SnotifyToastConfig
} from 'ng-snotify';

import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';

import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { AuthenticationService } from './_shared';
import { NotificationService } from './_shared';
import { LogoutService } from './_services/logout/logout.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  style = 'material';
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

  constructor(
    private snotifyService: SnotifyService,
    private router: Router,
    private idle: Idle,
    private keepalive: Keepalive,
    private authService: AuthenticationService,
    private notificationService: NotificationService,
    private logoutService: LogoutService
  ) {
    // sets an idle timeout of 10 mins
    idle.setIdle(600);
    // sets a timeout period of 5 mins. after 10 mins of inactivity,
    // the user will be considered timed out.
    idle.setTimeout(300);
    // sets the default interrupts, in this case, things like clicks,
    // scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => (this.idleState = 'No longer idle.'));
    idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      this.logoutService.logOut().subscribe(res => {
        this.notificationService.onSuccess('Good bye Session timeout...');
        this.authService.logout();
        localStorage.removeItem('user');
      });
    });
    idle.onIdleStart.subscribe(() => (this.idleState = 'You have gone idle!'));
    idle.onTimeoutWarning.subscribe(
      countdown =>
        (this.idleState = 'You will time out in ' + countdown + ' seconds!')
    );

    // sets the ping interval to 15 seconds
    keepalive.interval(15);

    keepalive.onPing.subscribe(() => (this.lastPing = new Date()));

    this.reset();
  }

  ngOnInit() {}

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
}
