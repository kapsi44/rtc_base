import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { PerfectScrollbarModule, PerfectScrollbarConfigInterface, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import 'zone.js/dist/zone';

// Lib

import { ToolbarModule } from '../../lib/toolbar';
import { ModalModule } from '../../lib//modal';
import { ModalEditFormModule } from '../../lib/modal-edit-form';
import { RowMenuModule } from '../../lib/row-menu';
import { ContextMenuModule } from '../../lib/context-menu';
import { DeviceModule } from '../device/device.module';

// Shared Modules
import {
  AuthenticationModule,
  EnvironmentModule,
  FormValidationModule,
  NotificationModule } from '../_shared';

  // Material Modules
import {
    MatCardModule,
    MatListModule,
    MatSidenavModule,
    MatSliderModule,
    MatProgressBarModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatChipsModule,
    MatFormFieldModule,
    MatTabsModule,
    MatGridListModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatNavList,
    MatExpansionModule
} from '@angular/material';

import { MDBBootstrapModule } from 'angular-bootstrap-md';


// Component
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogoutComponent } from '../_auth/logout/logout.component';
import { NavHeaderComponent } from './nav-header/nav-header.component';
import { RegisterComponent } from '../_auth/register/register.component';
import { LoginComponent } from '../_auth/login/login.component';
import { AdminComponent } from '../admin/admin.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

@NgModule({
  declarations: [
    DashboardComponent,
    NavHeaderComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    AdminComponent,
  ],
  imports: [
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    AuthenticationModule,
    EnvironmentModule,
    FormValidationModule,
    NotificationModule,
    RouterModule,
    DeviceModule,
    // mat-modules
    MatCardModule,
    MatListModule,
    MatSidenavModule,
    MatSliderModule,
    MatProgressBarModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatChipsModule,
    MatFormFieldModule,
    MatTabsModule,
    MatGridListModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    // mdb
    MDBBootstrapModule.forRoot(),
    ToolbarModule,
    ModalModule,
    ModalEditFormModule,
    RowMenuModule,
    ContextMenuModule,
    // idle user module
    NgIdleKeepaliveModule.forRoot()
  ],
  providers: [
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class CoreModule { }
