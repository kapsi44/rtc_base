import { Injectable } from '@angular/core';
import { EnvironmentService } from '../../_shared/environment/environment.service';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root',
})
export class LogoutService {
  constructor(
    private environmentService: EnvironmentService,
    private apiService: ApiService,
  ) {}

  logOut() {
    let url = this.environmentService.setApiService('logout');
    return this.apiService.post(url);
  }
}
