import { Injectable } from '@angular/core';
import { EnvironmentService } from '../../_shared/environment/environment.service';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoginService {

  constructor(
    private environmentService: EnvironmentService,
    private apiService: ApiService
  ) { }

  login(loginData): Observable<any> {
    let url = this.environmentService.setAuthService('oauth/token');
    return this.apiService.post(url, loginData);
  }

  getMe(): Observable<any> {
    let url = this.environmentService.setApiService('me');
    return this.apiService.get(url);
  }
}
