import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EnvironmentService } from '../../_shared/environment/environment.service';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  constructor(
    private environmentService: EnvironmentService,
    private apiService: ApiService
  ) {}

  register(registerData) {
    let url = this.environmentService.setApiService('register');
    return this.apiService.post(url, registerData);
  }
}
