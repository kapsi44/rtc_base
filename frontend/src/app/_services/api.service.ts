import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { TokenStorage } from '../_shared/authentication/token-storage.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';

import {
  Headers,
  Http,
  Response,
  URLSearchParams,
  ResponseContentType
} from '@angular/http';

@Injectable({
  providedIn: 'root',
})

export class ApiService {
  constructor(
    private http: HttpClient,
    private tokenService: TokenStorage,
  ) {}

  private setHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    if (this.getToken()) {
      headers = new HttpHeaders()
        .set('Authorization', this.getToken())
        .set('content-type', 'application/json')
        .set('Accept', 'application/json');
    } else {
      headers = new HttpHeaders()
        .set('content-type', 'application/json')
        .set('Accept', 'application/json');
    }

    return headers;
  }

  getToken(): any {
    return this.tokenService.getAccessToken();
  }

  get(
    path: string,
    params: URLSearchParams = new URLSearchParams()
  ): Observable<any> {
    return this.http
      .get(path, {
        headers: this.setHeaders()
      })
      .catch((error: any) => {
        this.handleError(error);
        return Observable.throw(error);
      })
      .map((response: Response) => {
        return response;
      });
  }

  put(
    path: string,
    body: Object = {},
    params: URLSearchParams
  ): Observable<any> {
    return this.http
      .put(path, JSON.stringify(body), {
        headers: this.setHeaders()
      })
      .catch((error: any) => {
        this.handleError(error);
        return Observable.throw(error);
      })
      .map((response: Response) => {
        return response;
      });
  }

  post(
    path: string,
    body: Object = {},
  ): Observable<any> {
    return this.http
      .post(path, JSON.stringify(body), {
        headers: this.setHeaders()
      })
      .catch((error: any) => {
        this.handleError(error);
        return Observable.throw(error);
      })
      .map((response: Response) => {
        return response;
      });
  }

  delete(path): Observable<any> {
    return this.http
      .delete(path, { headers: this.setHeaders() })
      .catch((error: any) => {
        this.handleError(error);
        return Observable.throw(error);
      })
      .map((response: Response) => {
        return response;
      });
  }

  handleError(error): void {
    let errorMessage = '';
    if (error.status === 404) {
      errorMessage = 'The System seems to be down. Please try again later.';
    } else if (error.status === 500) {
      errorMessage = 'There is an error in the system. Please try again later.';
    } else if (error.status === 400) {
      errorMessage = 'Invalid credentials.';
    }
  }
}
