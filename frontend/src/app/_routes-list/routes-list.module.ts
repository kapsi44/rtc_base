import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules, Routes } from '@angular/router';
import { PublicGuard, ProtectedGuard } from 'ngx-auth';
// import { AuthModule } from '../auth/auth.module';
import { LogoutComponent } from '../_auth/logout/logout.component';
import { LoginComponent } from '../_auth/login/login.component';
import { RegisterComponent } from '../_auth/register/register.component';
import { DashboardComponent } from '../core/dashboard/dashboard.component';
import {CustomFilterComponent} from '../device/custom-filter/customFilter.component';
// import { AuthComponent } from '../auth/auth.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  { path: 'login', canActivate: [PublicGuard], component: LoginComponent },
  { path: 'logout', canActivate: [ProtectedGuard], component: LogoutComponent },
  { path: 'dashboard', canActivate: [ProtectedGuard], component: DashboardComponent },
  { path: 'reports', canActivate: [ProtectedGuard], component: CustomFilterComponent },
  {
    path: 'register',
    component: RegisterComponent
  },
  { path: '**', pathMatch: 'full', redirectTo: '/' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: false, preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule]
})
export class RoutesListModule {}
