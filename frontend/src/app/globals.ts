import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
    user = JSON.parse(localStorage.getItem('user'));
}
