import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../_services/login/login.service';
import { AuthenticationService } from '../../_shared';
import { FormValidationService } from '../../_shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userForm: FormGroup;
  user = {'name': '', 'id': ''};
  message = '';
  errMsgArr = [];

  constructor(
    private spinnerService: Ng4LoadingSpinnerService,
    private loginService: LoginService,
    private authService: AuthenticationService,
    private router: Router,
    private formValidationService: FormValidationService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.userForm = this.fb.group({
      'username': ['', [
        Validators.required,
        Validators.email,
        ]
      ],
      'password': ['', [
        // Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25)
      ]
      ],
    });
  }

  get f() { return this.userForm.controls; }

  public login() {
    if (this.userForm.invalid) {
      return;
    }
    this.spinnerService.show();
    this.authService.login(this.userForm.value).subscribe((value) => {
      this.loginService.getMe().subscribe((value) => {
        this.user.name = value.data.name;
        this.user.id = value.data.id;
        localStorage.setItem('user', JSON.stringify(this.user));
        this.spinnerService.hide();
        this.router.navigate(['dashboard']);
      });
    }, err => {
      this.spinnerService.hide();
      if (err.status_code === 422) {
        this.errMsgArr = this.formValidationService.getErrors(err.errors);
      } else {
        this.errMsgArr = ['User Credentials are Incorrect'];
      }
    });
  }

}
