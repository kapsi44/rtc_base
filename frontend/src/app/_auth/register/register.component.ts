import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../../_services/register/register.service';
import { FormValidationService } from '../../_shared';
import { AuthenticationService } from '../../_shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userForm: FormGroup;

  constructor(
    private spinnerService: Ng4LoadingSpinnerService,
    private registerService: RegisterService,
    private formValidationService: FormValidationService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  message = '';
  errMsgArr = [];

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.userForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      c_password: ['', [Validators.required, Validators.minLength(6)]]
  });

  }

  get f() { return this.userForm.controls; }

  register() {
    if (this.userForm.invalid) {
      return;
    }
    this.spinnerService.show();
    this.registerService.register(this.userForm.value)
    .subscribe((value) => {
      this.spinnerService.hide();
      this.router.navigate(['login']);
    }, err => {
      this.spinnerService.hide();
      if (err.status_code === 422) {
        this.errMsgArr = this.formValidationService.getErrors(err.errors);
      } else {
        console.log('Error => ', err.message);
      }
    });
  }
}
