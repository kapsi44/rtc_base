import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { CoreModule } from './core/core.module';
import { RoutesListModule } from './_routes-list/routes-list.module';
import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Globals } from './globals';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    Ng4LoadingSpinnerModule.forRoot(),
    HttpModule,
    HttpClientModule,
    RoutesListModule,
    CoreModule,
    BrowserAnimationsModule,
    SnotifyModule,
    LayoutModule,
  ],
  providers: [
    {
      provide: 'SnotifyToastConfig',
      useValue: ToastDefaults,
    },
    SnotifyService,
    Globals
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
