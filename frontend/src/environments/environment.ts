// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: 'http://172.24.144.220:8080/',
  clientSecret: 'UeueoAPbu5tyD29z4BPJySokvAqJ12H6ighX1VIy',
  clientId: 2,
  grantType: 'password',
};

export const deviceData = {
  apiUrl: 'http://172.24.144.220:8080/api/device',
  assetUrl: 'assets/rtc_sample.json'
};
