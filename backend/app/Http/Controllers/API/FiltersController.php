<?php

namespace App\Http\Controllers\API;

use App\Filters;
use App\Http\Controllers\Controller;
use Debugbar;
use Illuminate\Http\Request;


class FiltersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id = null, Request $request)
    {
        try {
            $filter = isset($user_id) ? 
                        Filters::where('user_id', '=', $user_id) 
                        ->get()
                        : Filters::all();
            return response()->success($filter);
        } catch (Exception $e) {
            Debugbar::addThrowable($e);
            return response()->exception($e->getMessage(), $e->getCode());
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $filters = Filters::create($request->all());
            if ($filters) {
                return response()->success($filters, 200);
            } else {
                return response()->error($filters, 400);
            }
        } catch (Exception $e) {
            Debugbar::addThrowable($e);
            return response()->exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $filter = Filters::find($id);
            $res  = $filter->delete();
            return response()->success($res);
        } catch (Exception $e) {
            Debugbar::addThrowable($e);
            return response()->exception($e->getMessage(), $e->getCode());
        }
    }
    
}
