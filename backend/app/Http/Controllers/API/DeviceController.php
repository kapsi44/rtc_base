<?php

namespace App\Http\Controllers\API;

use App\Device;
use App\Http\Controllers\Controller;
use Debugbar;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = json_decode($request->get('data'),true);
        try {
            if ($data){
                return $this->filterDataArray($data);
            } else {
                $device = Device::all();
                return response()->json($device);
            }
        } catch (Exception $e) {
            Debugbar::addThrowable($e);
            return response()->exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'model'      => 'required',
                'category' => 'required',
            ]);
            $device = Device::create($request->all());
            if ($device) {
                return response()->success($device, 200);
            } else {
                return response()->error($device, 400);
            }
        } catch (Exception $e) {
            Debugbar::addThrowable($e);
            return response()->exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $device = Device::find($id);
            return response()->success($device);
        } catch (Exception $e) {
            Debugbar::addThrowable($e);
            return response()->exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            /*$device              = new device;
            $device->author      = $request->input('author');
            $device->description = $request->input('description');
            $device->save();*/
            $device = Device::findorFail($id);
            $device = $device->update($request->all());
            return response()->success($device);
        } catch (Exception $e) {
            Debugbar::addThrowable($e);
            return response()->exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $device = Device::find($id);
            $res  = $device->delete();
            return response()->success($res);
        } catch (Exception $e) {
            Debugbar::addThrowable($e);
            return response()->exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Filter the requested Query
     */

    public function filterDataArray($data)
    {
        try {
            $device = Device::query();
            if(isset($data[0]['platform'])) {
                $device = $device->whereIn('device->platform->name', $data[0]['platform']);
            } if(isset($data[0]['status'])) {
                $device = $device->whereIn('status',  $data[0]['status']);
            } if(isset($data[0]['country'])) {
                $device = $device->whereIn('device->locale->country', $data[0]['country']);
            }

            $results = $device->get();
            
            return response()->json($results);

        } catch (Exception $e) {
            Debugbar::addThrowable($e);
            return response()->exception($e->getMessage(), $e->getCode());
        }
    }
}