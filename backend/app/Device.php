<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = ['device', 'event_type', 'application', 'status'];

    protected $casts = [
        'device' => 'array',
        'application' => 'array',
        'attributes' => 'array'
    ];
}
